describe('template spec', () => {
    it('passes', () => {
      cy.visit('https://josenunhez-spotify-web.web.app/')
      cy.get('.top-songs-spain > :nth-child(2) > .song > .texto > .link > a')
      cy.get('.top-songs-spain > :nth-child(2) > .song > .texto > .link > a').click()
      cy.get('.top-songs-spain > :nth-child(2) > .song > .texto > .link > a').should('have.attr', 'href')
    })
  })