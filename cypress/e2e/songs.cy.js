describe('template spec', () => {
  it('passes', () => {
    cy.visit('https://josenunhez-spotify-web.web.app/')
    cy.get('[href="/artists"]').click()
    cy.get('#artist-input').type('The Beatles')
    cy.get('button').click()
    cy.get('h2').should('contain', 'The Beatles')
  })
})