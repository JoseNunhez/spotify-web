import { LitElement, html } from "lit";
import "./song.css";

export class SongUI extends LitElement {
  static get properties() {
    return {
      song: { type: Object },
      listaSpoty: { type: String }
    };
  }

  render() {
    return html`<ul class="song">
      <li>
        <img
          src=${this.song?.artistImage}
          alt="imagen de ${this.song?.artist}"
          width="100px"
        />
      </li>
      <div class="texto">
        <li class="name">${this.song?.name}</li>
        <li>Album: ${this.song?.album}</li>
        <li>Artista: ${this.song?.artist}</li>
        <li>Popularidad: ${this.song?.popularity}</li>
        <li id="${this.song?.id}-${this.listaSpoty}"><button @click=${this.handleClick}>Reproducir</button></li>
      </div>
    </ul>`;
  }

  createRenderRoot() {
    return this;
  }
}




customElements.define("song-ui", SongUI);
