export class Artist {
    constructor(id, rank, artist, lastWeek, peakPosition, weeksOnChart, detail) {
      this.id = id;
      this.rank = rank;
      this.artist = artist;
      this.lastWeek = lastWeek;
      this.peakPosition = peakPosition;
      this.weeksOnChart = weeksOnChart;
      this.detail = detail;
    }
  }
  