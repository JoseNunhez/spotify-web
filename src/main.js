import "./main.css";
import { Router } from "@vaadin/router";

import "./pages/home.page";
import "./pages/artist.page";


const outlet = document.querySelector("#outlet");
const router = new Router(outlet);

router.setRoutes([
  { path: "/", component: "home-page" },
  { path: "/artists", component: "artist-page" },
  { path: "(.*)", redirect: "/" },
]);
