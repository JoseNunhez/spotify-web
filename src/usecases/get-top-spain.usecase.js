import { SongsSpainRepository } from "../repositories/songs-spain.repository";

export class GetSongsSpainUseCase {
    static async execute() {
        const repository = new SongsSpainRepository();
        const response = await repository.getSongsSpain();
        console.log(response)
        return response;
    }
}

