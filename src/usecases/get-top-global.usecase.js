import { SongsGlobalRepository } from "../repositories/songs-global.repository";

export class GetSongsGlobalUseCase {
    static async execute() {
        const repository = new SongsGlobalRepository();
        const response = await repository.getSongsGlobal();
        console.log(response)
        return response;
    }
}

