import { SongsArtistRepository } from "../repositories/artists.repository";

export class GetSongsArtistUseCase {
    static async execute(artist) {
        const repository = new SongsArtistRepository();
        const response = await repository.getSongsArtist(artist);
        console.log(response)
        return response;
    }
}

