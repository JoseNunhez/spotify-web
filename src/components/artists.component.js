import { LitElement, html } from "lit";
import { GetSongsArtistUseCase } from "../usecases/get-songs-artist.usecase";
import "./../ui/song.ui";

export class ArtistComponent extends LitElement {
  static get properties() {
    return {
      songs: { type: Array },
      artist: { type: String },
    };
  }

  constructor() {
    super();
    this.songs = [];
    this.artist = "Taylor Swift";
  }

  async loadSongs(artist) {
    this.songs = await GetSongsArtistUseCase.execute(artist);
  }

  async connectedCallback() {
    super.connectedCallback();
    this.loadSongs(this.artist);
  }

  render() {
    return html`
      <div id="buscador-artista">
        <label for="artist-input">Artist:</label>
        <input id="artist-input" type="text" placeholder="Taylor Swift" />
        <button @click="${this.handleSearch}">Search</button>
      </div>
      <h2>Top Songs of ${this.artist}</h2>
      <div class="top-artists">
        ${this.songs.map(
          (song) => html`
            <song-ui
              @click=${(e) => this.handleClick(e, song, "artist")}
              .song=${song}
              .listaSpoty=${"artist"}
            ></song-ui>
          `
        )}
      </div>
    `;
  }

  
  //Añadir un iframe con la canción
  handleClick(e, song, listaSpoty) {
    console.log("cancion", song);
    const iframe = document.createElement("iframe");
    iframe.src = `https://open.spotify.com/embed/track/${song.id}`;
    iframe.width = "300";
    iframe.height = "80";
    iframe.frameborder = "0";
    iframe.allowtransparency = "true";
    iframe.allow = "encrypted-media";
    document.getElementById(`${song.id}-${listaSpoty}`).innerHTML = "";
    document.getElementById(`${song.id}-${listaSpoty}`).appendChild(iframe);
  }
  handleSearch() {
    const artistInput = document.querySelector("#artist-input");
    const artist = artistInput.value;
    this.artist = artist;
    this.loadSongs(artist);
  }
  
  createRenderRoot() {
    return this;
  }
}

customElements.define("article-artist", ArtistComponent);
