import { LitElement, html } from "lit";
import { GetSongsSpainUseCase } from "../usecases/get-top-spain.usecase";
import { GetSongsGlobalUseCase } from "../usecases/get-top-global.usecase";
import "./../ui/song.ui";

export class TopSongsComponent extends LitElement {
  static get properties() {
    return {
      songsSpain: { type: Array },
      songsGlobal: { type: Array },
    };
  }
  constructor() {
    super();
    this.songsSpain = [];
    this.songsGlobal = [];
  }
  async loadSongs() {
    this.songsSpain = await GetSongsSpainUseCase.execute();
    this.songsGlobal = await GetSongsGlobalUseCase.execute();
  }
  //Volver a renderizar un componente al ejecutar una función
  

  async connectedCallback() {
    super.connectedCallback();
    this.loadSongs();
  }
  render() {
    return html`
      <div class="top-songs-spain">
        <h2>Top Spain</h2>
        ${this.songsSpain.map(
          (song) => html`<song-ui
          @click=${(e) => this.handleClick(e, song, "spain")}
          .song=${song}
          .listaSpoty=${"spain"}
        ></song-ui>`
        )}
      </div>

      <div class="top-songs-global">
        <h2>Top Global</h2>
        ${this.songsGlobal.map(
          (song) =>
            html`<song-ui
              @click=${(e) => this.handleClick(e, song, "global")}
              .song=${song}
              .listaSpoty=${"global"}
            ></song-ui>`
        )}
      </div>
    `;
  }
  handleClick(e, song, listaSpoty) {
  console.log(listaSpoty)
  const iframe = document.createElement("iframe");
  iframe.setAttribute("src", `https://open.spotify.com/embed/track/${song.id}`);
  iframe.setAttribute("width", "300");
  iframe.setAttribute("height", "80");
  iframe.setAttribute("frameborder", "0");
  iframe.setAttribute("allowtransparency", "true");
  iframe.setAttribute("allow", "encrypted-media");
  document.getElementById(`${song.id}-${listaSpoty}`).innerHTML = "";
  document.getElementById(`${song.id}-${listaSpoty}`).appendChild(iframe);
}
  createRenderRoot() {
    return this;
  }
}

customElements.define("top-songs", TopSongsComponent);
