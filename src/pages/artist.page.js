import "../components/artists.component.js";

export class ArtistPage extends HTMLElement {
    connectedCallback() {
      this.innerHTML = `<article-artist></article-artist>`;
  }
}
  
  customElements.define("artist-page", ArtistPage);
  