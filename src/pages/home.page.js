import "./../components/top-songs.component.js";

export class HomePage extends HTMLElement {
    connectedCallback() {
      this.innerHTML = `<top-songs></top-songs>`;
    }
  }
  
  customElements.define("home-page", HomePage);
  