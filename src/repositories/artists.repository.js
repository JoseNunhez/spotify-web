const axios = require("axios");
const querystring = require('querystring');

export class SongsArtistRepository {
  async getSongsArtist(artist) {
    // Obteniendo el token de acceso de Spotify
    console.log(artist)
    const CLIENT_ID = 'b129dbbab01b400288d1fc510de71c72';
    const CLIENT_SECRET = '8afc950a4a2f4fff8b1ccab55f0edb88';
    const AUTH_URL = "https://accounts.spotify.com/api/token";

    const getToken = async () => {
        const authString = `${CLIENT_ID}:${CLIENT_SECRET}`;
        const encodedAuthString = btoa(authString);
        const response = await axios.post(
          AUTH_URL,
          querystring.stringify({
            grant_type: "client_credentials",
          }),
          {
            headers: {
              "Content-Type": "application/x-www-form-urlencoded",
              Authorization: `Basic ${encodedAuthString}`,
            },
          }
        );
        return response.data.access_token;
      };
    const songsArtist = async (artist) => {
      const token = await getToken();
      console.log(token);
      const API_URL = "https://api.spotify.com/v1/search";

      // Obteniendo el top 10 canciones de un artista en Spotify

      const response = await axios.get(API_URL, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
        params: {
          q: artist,
          type: "artist",
        },
      });
      const artistId =
        response.data.artists.items.length > 0
          ? response.data.artists.items[0].id
          : null;

      if (!artistId) {
        console.log(`No se encontró al artista ${artist}`);
        return null;
      }

      const TOP_TRACKS_URL = `https://api.spotify.com/v1/artists/${artistId}/top-tracks?market=US`;
      const topTracksResponse = await axios.get(TOP_TRACKS_URL, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
        params: {
          limit: 10,
        },
      });
      const topTracks =
      topTracksResponse.data.tracks.map((track) => ({
        name: track.name,
        album: track.album.name,
        popularity: track.popularity,
        artist: track.artists[0].name,
        artistImage: track.album.images[0].url,
        url: track.external_urls.spotify,
        id: track.id
      })) || [];
      
      console.log("canciones", topTracksResponse.data.tracks[0].external_urls.spotify)
      console.log(topTracks);

      return topTracks;
    };
    return songsArtist(artist);
  }
}
