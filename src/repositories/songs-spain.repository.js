const axios = require("axios");
const querystring = require('querystring');

export class SongsSpainRepository {
  async getSongsSpain() {
    // Obteniendo el token de acceso de Spotify
    console.log()
    const CLIENT_ID = 'b129dbbab01b400288d1fc510de71c72';
    const CLIENT_SECRET = '8afc950a4a2f4fff8b1ccab55f0edb88';
    const AUTH_URL = "https://accounts.spotify.com/api/token";

    const getToken = async () => {
        const authString = `${CLIENT_ID}:${CLIENT_SECRET}`;
        const encodedAuthString = btoa(authString);
        const response = await axios.post(
          AUTH_URL,
          querystring.stringify({
            grant_type: "client_credentials",
          }),
          {
            headers: {
              "Content-Type": "application/x-www-form-urlencoded",
              Authorization: `Basic ${encodedAuthString}`,
            },
          }
        );
        return response.data.access_token;
      };
    const songsSpain = async () => {
      const token = await getToken();

      // Obteniendo el top 10 canciones de un artista en Spotify

      const TOP_TRACKS_URL = `https://api.spotify.com/v1/playlists/37i9dQZEVXbNFJfN1Vw8d9`;
      const topTracksResponse = await axios.get(TOP_TRACKS_URL, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
        params: {
          limit: 20,
        },
      });

      console.log("canciones", topTracksResponse.data.tracks.items)

      const topTracks =
        topTracksResponse.data.tracks.items.map((song) => ({
          name: song.track.name,
          album: song.track.album.name,
          popularity: song.track.popularity,
          artist: song.track.artists[0].name,
          artistImage: song.track.album.images[0].url,
          url: song.track.external_urls.spotify,
          id: song.track.id
        })) || [];

      console.log(topTracks);

      return topTracks;
    };
    return songsSpain();
  }
}
