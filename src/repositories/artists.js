export const SongsQueen = [
    { 
      album: "A Night At The Opera (Deluxe Remastered Version)",
      artist: "Queen",
      artistImage: "https://i.scdn.co/image/ab67616d0000b273ce4f1737bc8a646c8c4bd25a",
      name: "Bohemian Rhapsody - Remastered 2011",
      popularity: 74
    },
    { 
      album: "Jazz (Deluxe Remastered Version)",
      artist: "Queen",
      artistImage: "https://i.scdn.co/image/ab67616d0000b273008b06ec71019afd70153889",
      name: "Don't Stop Me Now - Remastered 2011",
      popularity: 75
    },
    // y así sucesivamente para los otros objetos
  ];