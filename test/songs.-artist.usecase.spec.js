const { SongsArtistRepository } = require('../src/repositories/artists.repository');

describe('SongsArtistRepository', () => {
  let repository;

  beforeEach(() => {
    repository = new SongsArtistRepository();
  });

  describe('getSongsArtist', () => {
    it('should return the top 10 tracks for a valid artist name', async () => {
      const artist = 'The Beatles';
      const topTracks = await repository.getSongsArtist(artist);
      expect(topTracks).toBeDefined();
      expect(topTracks.length).toBe(10);
      expect(topTracks[0].name).toBeDefined();
      expect(topTracks[0].artist).toBe('The Beatles');
    });
  });
});