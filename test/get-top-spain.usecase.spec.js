import { GetSongsSpainUseCase } from '../src/usecases/get-top-spain.usecase';

describe('GetSongsSpainUseCase', () => {
  it('should return an array of top 20 songs in Spain', async () => {
    const response = await GetSongsSpainUseCase.execute();
    expect(response).toHaveLength(50);
    response.forEach(song => {
      expect(song).toHaveProperty('name');
      expect(song).toHaveProperty('album');
      expect(song).toHaveProperty('popularity');
      expect(song).toHaveProperty('artist');
      expect(song).toHaveProperty('artistImage');
      expect(song).toHaveProperty('url');
      expect(song).toHaveProperty('id');
    });
  });
});
