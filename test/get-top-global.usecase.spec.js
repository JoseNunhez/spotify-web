import { GetSongsGlobalUseCase } from "../src/usecases/get-top-global.usecase";
import { SongsGlobalRepository } from "../src/repositories/songs-global.repository";

jest.mock("../src/repositories/songs-global.repository");

describe("GetSongsGlobalUseCase", () => {
  it("should return an array of global songs", async () => {
    const songs = [
      {
        name: "Song 1",
        album: "Album 1",
        popularity: 80,
        artist: "Artist 1",
        artistImage: "image-url-1",
        url: "https://open.spotify.com/track/1",
      },
      {
        name: "Song 2",
        album: "Album 2",
        popularity: 75,
        artist: "Artist 2",
        artistImage: "image-url-2",
        url: "https://open.spotify.com/track/2",
      },
    ];

    const getSongsGlobalMock = jest.fn(() => songs);
    SongsGlobalRepository.prototype.getSongsGlobal = getSongsGlobalMock;

    const response = await GetSongsGlobalUseCase.execute();

    expect(getSongsGlobalMock).toHaveBeenCalledTimes(1);
    expect(response).toEqual(songs);
  });
});
